﻿//////////////////////////////////////////////////////////////////////
// Provides the framework for a raytracer.
////////////////////////////////////////////////////////////////////////

#include <vector>

#ifdef _WIN32
    // Includes for Windows
    #include <windows.h>
    #include <cstdlib>
    #include <limits>
    #include <crtdbg.h>
#else
    // Includes for Linux
#endif

#include "geom.h"
#include "raytrace.h"
#include "Minimizer.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


#define TRACEPATH 1
#define IMAGE 0
#define NORMALS 0
#define DEPTH 0
#define DEBUGLOOP 1

//KD tree
#include <Eigen/StdVector>
#include <Eigen_unsupported/Eigen/BVH>

#include "RNG.h"
//// A good quality *thread-safe* Mersenne Twister random number generator.
//#include <random>
//std::mt19937_64 RNGen;
//std::uniform_real_distribution<> myrandom(0.0, 1.0);
//// Call myrandom(RNGen) to get a uniformly distributed random number in [0,1].


#include <random>
std::mt19937_64 RNGen;
std::uniform_real_distribution<> myrandom(0.0, 1.0);

const float Radians = PI / 180.0f;    // Convert degrees to radians

Scene::Scene()
{ 
   // realtime = new Realtime(); 
}

void Scene::Finit()
{
	//KdBVH<float, 3, Shape*> Tree(shapes.begin(), shapes.end());
	NumberOfLights = lightObj.size();
}

void Scene::triangleMesh(MeshData* mesh) 
{ 
	for (auto &indices : mesh->triangles) 
	{
	
		Vector3f v0 = mesh->vertices[indices[0]].point;
		Vector3f v1 = mesh->vertices[indices[1]].point;
		Vector3f v2 = mesh->vertices[indices[2]].point;

		Vector3f n0 = mesh->vertices[indices[0]].normal;
		Vector3f n1 = mesh->vertices[indices[1]].normal;
		Vector3f n2 = mesh->vertices[indices[2]].normal;

		/*Vector2f t0 = mesh->vertices[indices[0]].texture;
		Vector2f t1 = mesh->vertices[indices[1]].texture;
		Vector2f t2 = mesh->vertices[indices[2]].texture;*/

		auto triangle = new Triangle(v0, v1, v2, n0, n1, n2,mesh->mat/*,t0,t1,t2*/);
		shapes.push_back(triangle);
	}

}

Quaternionf Orientation(int i, 
                        const std::vector<std::string>& strings,
                        const std::vector<float>& f)
{
    Quaternionf q(1,0,0,0); // Unit quaternion
    while (i<strings.size()) {
        std::string c = strings[i++];
        if (c == "x")  
            q *= angleAxis(f[i++]*Radians, Vector3f::UnitX());
        else if (c == "y")  
            q *= angleAxis(f[i++]*Radians, Vector3f::UnitY());
        else if (c == "z")  
            q *= angleAxis(f[i++]*Radians, Vector3f::UnitZ());
        else if (c == "q")  {
            q *= Quaternionf(f[i+0], f[i+1], f[i+2], f[i+3]);
            i+=4; }
        else if (c == "a")  {
            q *= angleAxis(f[i+0]*Radians, Vector3f(f[i+1], f[i+2], f[i+3]).normalized());
            i+=4; } }
    return q;
}

////////////////////////////////////////////////////////////////////////
// Material: encapsulates surface properties
void Material::setTexture(const std::string path)
{
    int width, height, n;
    stbi_set_flip_vertically_on_load(true);
    unsigned char* image = stbi_load(path.c_str(), &width, &height, &n, 0);

    // Realtime code below:  This sends the texture in *image to the graphics card.
    // The raytracer will not use this code (nor any features of OpenGL nor the graphics card).
    /*glGenTextures(1, &texid);
    glBindTexture(GL_TEXTURE_2D, texid);
    glTexImage2D(GL_TEXTURE_2D, 0, n, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 100);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (int)GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (int)GL_LINEAR_MIPMAP_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);*/

    stbi_image_free(image);
}

void Scene::Command(const std::vector<std::string>& strings,
                    const std::vector<float>& f)
{
    if (strings.size() == 0) return;
    std::string c = strings[0];
    
    if (c == "screen") 
	{
        // syntax: screen width height
       // realtime->setScreen(int(f[1]),int(f[2]));
        width = int(f[1]);
        height = int(f[2]); 
	}

    else if (c == "camera") {
        // syntax: camera x y z   ry   <orientation spec>
        // Eye position (x,y,z),  view orientation (qw qx qy qz),  frustum height ratio ry
       // realtime->setCamera(Vector3f(f[1],f[2],f[3]), Orientation(5,strings,f), f[4]);
		this->camera.setCamera(Vector3f(f[1], f[2], f[3]), Orientation(5, strings, f), f[4]);
	
	}

    else if (c == "ambient") {
        // syntax: ambient r g b
        // Sets the ambient color.  Note: This parameter is temporary.
        // It will be ignored once your raytracer becomes capable of
        // accurately *calculating* the true ambient light.
       // realtime->setAmbient(Vector3f(f[1], f[2], f[3])); 
	}

    else if (c == "brdf")  {
        // syntax: brdf  r g b   r g b  alpha
        // later:  brdf  r g b   r g b  alpha  r g b ior
        // First rgb is Diffuse reflection, second is specular reflection.
        // third is beer's law transmission followed by index of refraction.
        // Creates a Material instance to be picked up by successive shapes
        currentMat = new Material(Vector3f(f[1], f[2], f[3]), Vector3f(f[4], f[5], f[6]), f[7]); }

    else if (c == "light") {
        // syntax: light  r g b   
        // The rgb is the emission of the light
        // Creates a Material instance to be picked up by successive shapes
        currentMat = new Light(Vector3f(f[1], f[2], f[3])); }
   
    else if (c == "sphere") {
        // syntax: sphere x y z   r
        // Creates a Shape instance for a sphere defined by a center and radius
       // realtime->sphere(Vector3f(f[1], f[2], f[3]), f[4], currentMat); 
		Sphere* sp = new Sphere(Vector3f(f[1], f[2], f[3]), f[4], currentMat);
		if (currentMat->isLight())
		{
			lightObj.push_back(sp);
		}
		shapes.push_back(sp);
	}

    else if (c == "box") {
        // syntax: box bx by bz   dx dy dz
        // Creates a Shape instance for a box defined by a corner point and diagonal vector
       // realtime->box(Vector3f(f[1], f[2], f[3]), Vector3f(f[4], f[5], f[6]), currentMat); 
		Box* b = new Box(Vector3f(f[1], f[2], f[3]), Vector3f(f[4], f[5], f[6]), currentMat);
		
		/*if (currentMat->isLight)
		{
			lightObj.push_back(b);
		}
		else*/
			shapes.push_back(b);
	}

    else if (c == "cylinder") {
        // syntax: cylinder bx by bz   ax ay az  r
        // Creates a Shape instance for a cylinder defined by a base point, axis vector, and radius
        //realtime->cylinder(Vector3f(f[1], f[2], f[3]), Vector3f(f[4], f[5], f[6]), f[7], currentMat); 
		Cylinder* cylinder = new Cylinder(Vector3f(f[1], f[2], f[3]), Vector3f(f[4], f[5], f[6]), f[7], currentMat);
		
		/*if (currentMat->isLight())
		{
			lightObj.push_back(cylinder);
		}
		else*/
			shapes.push_back(cylinder);
	}

    else if (c == "capsule") {
        // syntax: cylinder bx by bz   ax ay az  r
        // Creates a Shape instance for a cylinder defined by a base point, axis vector, and radius
        //realtime->cylinder(Vector3f(f[1], f[2], f[3]), Vector3f(f[4], f[5], f[6]), f[7], currentMat); 
	}

    else if (c == "mesh") {
        // syntax: mesh   filename   tx ty tz   s   <orientation>
        // Creates many Shape instances (one per triangle) by reading
        // model(s) from filename. All triangles are rotated by a
        // quaternion (qw qx qy qz), uniformly scaled by s, and
        // translated by (tx ty tz) .
        Matrix4f modelTr = translate(Vector3f(f[2],f[3],f[4]))
                          *scale(Vector3f(f[5],f[5],f[5]))
                          *toMat4(Orientation(6,strings,f));
       
		//not needed now, uncomment after doing basic shapes
		
		ReadAssimpFile(strings[1], modelTr);  
	}
    else 
	{
        fprintf(stderr, "\n*********************************************\n");
        fprintf(stderr, "* Unknown command: %s\n", c.c_str());
        fprintf(stderr, "*********************************************\n\n");
    }
}

Color Scene::TracePath(const Ray& r,  KdBVH<float, 3, Shape*>& tree)
{
	Color C(0, 0, 0);
	Color W(1,1,1);
	//Initial ray
	IntersectionData P,Q;
	Minimizer mini(r);
	BVMinimize(tree, mini);
	P = mini.iD;

#if DEBUGLOOP	
	if (!P.shape)
	{
		return Color(0, 0, 0);
	}
	else if (P.shape->m_pMaterial->isLight())
	{
		std::cout << P.shape->m_shapeType<<std::endl;
		return Radiance(P);
	}
	//Vector3f N = Vector3f(fabsf(P.N.x()) , fabsf (P.N.y()) , fabsf(P.N.z()));
	Vector3f N = P.N;
	while (myrandom(RNGen) <= 0.8f)
	{
		Vector3f wi;

		//explicit light connection
		//IntersectionData L = SampleLight();
		//float p = PdfLight(L) / GeometryFactor(P, L);
		//wi = L.P - P.P;
		//wi.normalize();
		//Ray new_ray(P.P, wi);
		//Minimizer mini(new_ray);
		//IntersectionData I;
		//I = mini.iD;
		//if (p > 0 && I.t != INFINITY && I.t != 0 && mini.iD.P == L.P) //Same point or same light??
		//{
		//	Color f = fabs(N.dot(wi))*EvalBrdf(I);
		//	AccumulatedLight += AccumulatedWeight * (f / p)*Radiance(L);
		//}

		
		//extend path
		wi = SampleBrdf(N);
		wi.normalize();
		Ray new_ray2(P.P,wi);
		Minimizer mini2(new_ray2);
		BVMinimize(tree, mini2);
		Q = mini2.iD;
		
		if(!Q.shape)
			break;

		float N_wi = fabs(N.dot(wi));
		Color f = N_wi * EvalBrdf(P);
		//std::cout << "EvalBrdf : (x=" << EvalBrdf(P).x() << " y= " << EvalBrdf(P).y() << " z= " << EvalBrdf(P).z()<<"\n";
		float p2 = PdfBrdf(N,wi)*0.8;

		if (p2 < FLT_EPSILON)
			break;
		
		W *= f / p2;
		
		//implicit light connection
		if (Q.shape->m_pMaterial->isLight())
		{
			//std::cout << "Accumulated Weight : (x= " << W.x() << " , y = " << W.y() << ", z = " << W.z()<<"\n";
			C += W * Radiance(Q);
			//std::cout << "Accumulated Light : (x= " << C.x() << " , y = " << C.y() << ", z = " << C.z() << "\n";
			break;
		}
		
		//step frwd
		P = Q;

	}
	//return Color(0, 1, 0);
     return C;
#else
	return P.shape->m_pMaterial->Kd;
#endif
}

void Scene::TraceImage(Color* image, const int pass)
{
   
	float dx, dy, rx= camera.ry*((float)width/height);
	Vector3f X = rx * camera.orientation._transformVector(Vector3f::UnitX());
	Vector3f Y = camera.ry * camera.orientation._transformVector(Vector3f::UnitY());
	Vector3f Z = -1 * camera.orientation._transformVector(Vector3f::UnitZ());


	//load shapes in kd tree
	KdBVH<float, 3, Shape*> Tree(shapes.begin(), shapes.end());

	int passes = 512;
	int i = 0;
#if TRACEPATH
	while (++i<passes) 
	{
#endif
		//calculates rays for every pixel
		//if(i%50==0)
		std::cout << "Pass : " << i << "-----------------------------------------------------------------------------"<<std::endl;
#pragma omp parallel for schedule(dynamic, 1) // Magic: Multi-thread y loop
		for (int y = 0; y < height; y++)
		{
			//fprintf(stderr, "Rendering %4d\r", y);
			for (int x = 0; x < width; x++)
			{
				float dx = (2 * (x + myrandom(RNGen))) / ((float)(width)) - 1;
				float dy = (2 * (y + myrandom(RNGen))) / ((float)(height)) - 1;
				float t;
				Vector3f rayDir = dx * X + dy * Y + Z;
				rayDir.normalize();
				Ray r(camera.eye, rayDir);

//				for (int i = 0; i < shapes.size(); i++)
//				{
//					if (shapes[i]->intersect(r, iD))
//					{
//
//						if (iD.t < tempt) {
//							iD.N.normalize();
//							tempt = iD.t;
//#if DEPTH
//							t = iD.t;
//							t = fabs((t - 5.0f) / 4.0f);
//							image[y*width + x] = Color(t, t, t);
//
//#elif NORMALS
//							image[y*width + x] = Color(fabs(iD.N.x()), fabs(iD.N.y()), fabs(iD.N.z()));
//#else
//							image[y*width + x] = shapes[i]->m_pMaterial->Kd;
//#endif
//						}
//					}
//				}
#if TRACEPATH
				image[y*width + x] += TracePath(r, Tree);
				/*if(i%10==0)
					image[y*width + x] = image[y*width + x] /(float)i;*/
#endif
				/*float tempt = INFINITY;*/
				IntersectionData iD;
				Minimizer mini(r);
				float minDist = BVMinimize(Tree, mini);
				t = minDist;
				t = fabs((t - 5.0f) / 4.0f);
				mini.iD.N.normalize();
#if NORMALS
				image[y*width + x] = Color(fabs(mini.iD.N.x()), fabs(mini.iD.N.y()), fabs(mini.iD.N.z()));

#elif DEPTH
				image[y*width + x] = Color(t, t, t);
#elif IMAGE
				image[y*width + x] = mini.iD.shape->m_pMaterial->Kd;
#endif

//#else
//				

			}
		}
#if TRACEPATH
	}
#endif
    fprintf(stderr, "\n");

}

float Scene::GeometryFactor(const IntersectionData &A, const IntersectionData &B)
{
	Vector3f D = A.P - B.P;
	return fabs(  (A.N.dot(D)*B.N.dot(D))  /  D.dot(D)*D.dot(D)  );
}

Vector3f Scene::SampleLobe(Vector3f N, float c, float angleAroundN)
{
	//Here, c specifies the cosine of the angle between the returned vector and N, while ϕ gives an angle around N.
	float s = sqrtf(1-c*c);
	Vector3f K(s*cosf(angleAroundN),s*sinf(angleAroundN),c);
	Quaternionf q = Quaternionf::FromTwoVectors(Vector3f::UnitZ(), N); // quat to rotate Z to N

	return q._transformVector(K);  // K rotated to N's frame
}

float Scene::PdfLight(IntersectionData &Q)
{
	return 1/(float)(Q.shape->AreaOfLight()*NumberOfLights);
}

Color Scene::Radiance(IntersectionData &iD)
{
	return iD.shape->m_pMaterial->Kd;
}

Vector3f Scene::SampleBrdf(Vector3f &N)
{
	float random1 = myrandom(RNGen);
	float random2 = myrandom(RNGen);

	return SampleLobe(N,sqrtf(random1),(float)2*PI*random2);
}

float Scene::PdfBrdf(Vector3f &N, Vector3f &wi)
{
	return fabs(wi.dot(N))/PI;
}



IntersectionData Scene::SampleLight()
{
	int random = rand()%NumberOfLights;
	IntersectionData randomPoint = lightObj[random]->SampleSphere(lightObj[random]->m_center, lightObj[random]->m_radius);

	/*IntersectionData iD;
	iD.shape = lightObj[random];
	iD.P = randomPoint;
	iD.N = lightObj[random].normal;*/

	return randomPoint;
}

Color Scene::EvalBrdf(IntersectionData& iD )
{
	return iD.shape->m_pMaterial->Kd/PI;
}

Bbox bounding_box(Shape* shape) {

	return shape->BoundingBox();
}


