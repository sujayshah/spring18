#include "Intersections.h"
#include "Shape.h"



Interval::Interval()
{
	t0 = 0.0f;
	t1 = INFINITY;
}

Interval::Interval(float _t0, float _t1)
{
	t0 = _t0;
	t1 = _t1;
}

Interval::Interval(float t0, float t1, Vector3f N0, Vector3f N1)
{
	float temp;
	Vector3f Ntemp;
	if (t0 > t1)
	{
		temp = t0;
		t0 = t1;
		t1 = temp;

		Ntemp = N0;
		N0 = N1;
		N1 = Ntemp;

		this->t0 = t0;
		this->t1 = t1;
		this->N0 = N0;
		this->N1 = N1;
	}
}
//1 more constructor needed?

void Interval::Empty()
{
	this->t0 = 1;
	this->t1 = 0;
}

void Interval::intersect(const Ray & ray, Slab & slab)
{
	float temp; 
	N0 = -slab.N;
	N1 =  slab.N;

	if (slab.N.dot(ray.D)!=0)
	{
		t0 = -(slab.d0 + slab.N.dot(ray.Q)) / slab.N.dot(ray.D);
		t1 = -(slab.d1 + slab.N.dot(ray.Q)) /slab.N.dot(ray.D);

		if (t0 > t1)
		{
			temp = t0;
			t0 = t1;
			t1 = temp;
		}
	}
	else
	{
		float s0, s1;
		s0 = slab.N.dot(ray.Q)+  slab.d0;
		s1 = slab.N.dot(ray.Q) + slab.d1;

		if ((s0 < 0 && s1>0) || (s0 > 0 && s1 < 0))
		{
			this->t0 = 0;
			this->t1 = INFINITY;
		}
		else
			this->Empty();
	}
}

void Interval::intersect(const Ray & ray, Slab & slab,bool& inBox)
{
	float temp;
	N0 = -slab.N;
	N1 = slab.N;

	if (slab.N.dot(ray.D) != 0)
	{
		t0 = -(slab.d0 + slab.N.dot(ray.Q)) / slab.N.dot(ray.D);
		t1 = -(slab.d1 + slab.N.dot(ray.Q)) / slab.N.dot(ray.D);

		if (t0 > t1)
		{
			temp = t0;
			t0 = t1;
			t1 = temp;
		}
	}
	else
	{
		float s0, s1;
		s0 = slab.N.dot(ray.Q) + slab.d0;
		s1 = slab.N.dot(ray.Q) + slab.d1;

		if ((s0 < 0 && s1>0) || (s0 > 0 && s1 < 0))
		{
			inBox = true;
			this->t0 = 0;
			this->t1 = 0;
		}
		else
			this->Empty();
	}
}