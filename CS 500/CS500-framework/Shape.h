#pragma once
#include "geom.h"
#include "Ray.h"
#include "Intersections.h"

class Material;
class IntersectionData;
struct CompareIntervalMaxt0
{
	bool operator()(const Interval& a, const Interval& b)
	{
		return a.t0 < b.t0;
	}
};
struct CompareIntervalMint1
{

	bool operator()(const Interval& a, const Interval& b)
	{
		return a.t1 < b.t1;
	}
};
class Shape
{
public:
	Shape() {};
	Shape(Material* _mat);
	virtual ~Shape();
	virtual bool intersect(const Ray&, IntersectionData&) = 0;
	virtual Bbox BoundingBox() = 0;
	virtual float AreaOfLight()=0;
	Bbox m_boundingBox;
	std::string m_shapeType;
	Vector2f UV;
	Material* m_pMaterial;

};

class Sphere : public Shape
{
public:
	float m_radius;
	Vector3f m_center;
	Sphere(Vector3f _center, float _r, Material* _mat);
	~Sphere() {};
	bool intersect(const Ray&, IntersectionData&);
	float AreaOfLight();
	Bbox BoundingBox();
	float theta;
	float phi;
	
	//Choose a uniformly distributed point on a sphere: 
	IntersectionData SampleSphere(Vector3f, float);
	//other val calculation?
};
class Triangle : public Shape
{
public:
	Triangle(Vector3f v1, Vector3f v2, Vector3f v3, Vector3f n1, Vector3f n2, Vector3f n3 , Material* mat) :Shape(mat), m_V0(v1), m_V1(v2), m_V2(v3),
		m_N0(n1), m_N1(n2), m_N2(n3) {
		m_shapeType = "triangle";
	}

	Triangle(Vector3f v1, Vector3f v2, Vector3f v3, Vector3f n1, Vector3f n2, Vector3f n3, Material* mat, Vector2f t1, Vector2f t2, Vector2f t3);
	~Triangle() {}
	Vector3f m_V0, m_V1, m_V2;
	Vector3f m_N0, m_N1, m_N2;
	Vector3f m_t0, m_t1, m_t2;

	bool intersect(const Ray&, IntersectionData&);
	float AreaOfLight();
	Bbox BoundingBox();
	//other calculations?
	
};
class Slab
{
public:
	Slab() {};
	Slab(Vector3f,float, float);
	Vector3f N;
	float d0, d1;
	float tan;
	
};

class Box : public Shape
{
private:
	Slab slab[3];
public:
	Box(Vector3f,Vector3f,Material*);
	~Box() {};
	Vector3f corner;
	Vector3f diagonal;

	bool intersect(const Ray&, IntersectionData&);
	float AreaOfLight();
	Bbox BoundingBox();
	//other calculations?
};

class Cylinder:public Shape
{
public:
	Cylinder(Vector3f,Vector3f,float,Material *);
	~Cylinder() {};
	Vector3f base;
	Vector3f axis;
	float radius;

	bool intersect(const Ray&, IntersectionData&);
	float AreaOfLight();
	float theta;
	Bbox BoundingBox();


};