#pragma once

#include "Ray.h"
#include "Shape.h"
#include "Intersections.h"
class Minimizer
{
public:
	Minimizer();
	~Minimizer() {};

public:
	typedef float Scalar; // KdBVH needs Minimizer::Scalar defined 
	Ray ray;
	//Stuff to track the minimal t and its intersection info
	float tmin;
	IntersectionData iD;

	Minimizer(const Ray& r);
	

	float minimumOnObject(Shape* obj);


	float minimumOnVolume(const Bbox& box);
	
};

