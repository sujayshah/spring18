#include "Shape.h"
//#include <windows.h>
#include "Intersections.h"
#include <algorithm>
#include "RNG.h"
//#include "geom.h"

#include <random>
std::mt19937_64 RNGen2;
std::uniform_real_distribution<> myrandom2(0.0, 1.0);

Shape::Shape(Material* _mat)
{
	m_pMaterial = _mat;
}


Shape::~Shape()
{
}

//   SPHERE
//----------------------------------------------------------------------------
Sphere::Sphere(Vector3f _center, float _r, Material * _mat)
{
	m_center = _center;
	m_radius = _r;
	m_pMaterial = _mat;
	m_shapeType = "Sphere";
}

bool Sphere::intersect(const Ray& r, IntersectionData & iD)
{
	//intersection calculations
	Vector3f Q_ = r.Q - m_center;

	//r.D.normalize();
	float dis,delta, t1, t2;

	float dt=  Q_.dot(Q_);
	float st = Q_.dot(r.D);
	dis = (Q_.dot(r.D)*Q_.dot(r.D) - dt + m_radius * m_radius);

	if (dis < 0) {

		return false;
	}

	delta = sqrtf(dis);

	t1 = -Q_.dot(r.D) - delta;
	t2 = -Q_.dot(r.D) + delta;

	if ( t1 < 0 && t2 < 0)
	{
		return false;
	}

	if (t1 > 0 && t1 < t2) {
		iD.t = t1;
		iD.P = r.Q + iD.t * r.D;
		iD.N = iD.P - m_center;
		iD.N.normalize();
	}
	else if (t2 > 0 && t2 < t1) {
		iD.t = t2;
		iD.P = r.Q + iD.t * r.D;
		iD.N = iD.P - m_center;
		iD.N.normalize();
	}
	return true;

	//texture and angle 
	/*theta = atan2f(iD.N.y,iD.N.x);
	phi = acos(iD.N.z);

	UV.x = theta / ((float)2 * PI);
	UV.y= phi / PI;*/
}

float Sphere::AreaOfLight()
{
	return 4*PI*m_radius*m_radius;
}

Bbox Sphere::BoundingBox()
{
	//calculate bounding box
	m_boundingBox = Bbox(m_center - Vector3f(m_radius, m_radius, m_radius), m_center + Vector3f(m_radius, m_radius, m_radius));
	return m_boundingBox;

}

//Choose a uniformly distributed point on a sphere: 
IntersectionData Sphere::SampleSphere(Vector3f center, float radius)
{
	IntersectionData iD;
	float random1 = myrandom2(RNGen2);
	float random2 = myrandom2(RNGen2);

	float z = 2 * random1 - 1;
	radius = sqrtf(1 - z * z);

	float a = 2 * PI*random2;

	iD.N = Vector3f(radius*cosf(a), radius*sinf(a), z);
	iD.P = center + iD.N*radius;
	iD.shape = this;
	return iD;
}
//----------------------------------------------------------------------------

//TRIANGLE

Triangle::Triangle(Vector3f v1, Vector3f v2, Vector3f v3, Vector3f n1, Vector3f n2, Vector3f n3, Material * mat, Vector2f t1, Vector2f t2, Vector2f t3)
{
	m_V0 = v1;
	m_V1 = v2;
	m_V2 = v3;

	m_N0 = n1;
	m_N1 = n2;
	m_N2 = n3;

	m_pMaterial = mat;

	/*m_t0 = t1;
	m_t1 = t2;
	m_t2 = t3;*/
}

bool Triangle::intersect(const Ray &r, IntersectionData &iD)
{
	Vector3f E1 = m_V1 - m_V0;
	Vector3f E2 = m_V2 - m_V0;
	Vector3f p = r.D.cross(E2);
	float d = p.dot(E1);

	if (d == 0) // Ray is parallel to triangle 
		return false;

	Vector3f S = r.Q - m_V0;
	float u = (p.dot(S) / d);

	if (u < 0 || u>1) // Ray intersects plane, but outside E2 edge 
		return false;

	Vector3f q = S.cross(E1);
	float v = r.D.dot(q) / d;

	if (v < 0 || (u + v)>1)  // Ray intersects plane, but outside other edges 
		return false;

	float t = (E2.dot(q) / d);

	if (t < 0) // Ray's negative half intersects triangle 
		return false;
	else
	{
		iD.t = t;
		iD.P = r.Q + t * r.D;
		iD.N = E2.cross(E1);// (1 - u - v)*m_N0 + u * (m_N1)+v * (m_N2);//
		
		//other values
		//UV=(1 - u - v)*m_t0+u*m_t1+v*m_t2;
		iD.N = -iD.N;
		return true;
	}
}

float Triangle::AreaOfLight()
{
	/*Vector3f AB = m_V1 - m_V0;
	Vector3f AC = m_V2 - m_V0;

	float magAB = sqrtf(AB.x*AB.x+ AB.y*AB.y+ AB.z*AB.z);
	float magAC = sqrtf(AC.x*AC.x + AC.y*AC.y + AC.z*AC.z);
	float angle = acosf(AB.dot(AC)/(magAB*magAC));

	return 0.5f*magAB*magAC*sinf(angle);*/
	return 0.0f;
}

Bbox Triangle::BoundingBox()
{
	Vector3f v0, v1, v2;
	v0 = m_V0;
	v1 = m_V1;
	v2 = m_V2;
	Vector3f c1(std::min({ v0.x(),v1.x(),v2.x() }), std::min({ v0.y(),v1.y(),v2.y() }), std::min({ v0.z(),v1.z(),v2.z() }));
	Vector3f c2(std::max({ v0.x(),v1.x(),v2.x() }), std::max({ v0.y(),v1.y(),v2.y() }), std::max({ v0.z(),v1.z(),v2.z() }));

	m_boundingBox = Bbox(c1,c2);
	return m_boundingBox;
}



//AABB
bool Box::intersect( const Ray &r, IntersectionData &iD)
{
	float t0=0.0, t1= INFINITY;
	Interval interval[4];

	interval[0].t0 = t0;
	interval[0].t1 = INFINITY;

	Interval maxt0,mint1;
	for (int i = 1; i <=3; ++i)
	{
		interval[i].intersect(r, this->slab[i-1]);

	}

	maxt0 = interval[0];
	mint1 = interval[0];
	//last slab max calc problem
	for (int i = 0; i < 3; ++i)
	{
		maxt0 = std::max(interval[i + 1], maxt0,  CompareIntervalMaxt0());
		mint1 = std::min(mint1, interval[i + 1], CompareIntervalMint1());
		t0 = maxt0.t0;
		t1 = mint1.t1;
	}
	//t0 max=5.406
	//t1 min =5.593
	if (t0 < 0 && t1 < 0)
		return false;
	else if (t0 > t1)
		return false;
	else
	{
		if (t0 > 0 && t0 < t1) 
		{
			iD.t = t0;
			iD.P = r.Q + iD.t * r.D;
			iD.N = maxt0.N0;
		}
		else if (t1 > 0 && t1 < t0)
		{
			iD.t = t1;
			iD.P = r.Q + iD.t * r.D;
			iD.N = mint1.N1;
		}
		iD.N = -iD.N;
		return true;
	}
}

float Box::AreaOfLight()
{
	//float magDiagonal= diagonal.
	return 0.0f;
}

Bbox Box::BoundingBox()
{
	m_boundingBox = Bbox(corner, Vector3f(corner.x() + diagonal.x(), corner.y() + diagonal.y(), corner.z() + diagonal.z()));
	return m_boundingBox;
}


Box::Box(Vector3f _corner, Vector3f _diag, Material* _mat)
{
	corner = _corner;
	diagonal = _diag;
	m_pMaterial = _mat;

	slab[0].N = Vector3f(1.0,0.0,0.0);
	slab[0].d0 = -corner.x();
	slab[0].d1 = -corner.x() - diagonal.x();

	slab[1].N = Vector3f(0.0, 1.0, 0.0);
	slab[1].d0 = -corner.y();
	slab[1].d1 = -corner.y() - diagonal.y();

	slab[2].N = Vector3f(0.0, 0.0, 1.0);
	slab[2].d0 = -corner.z();
	slab[2].d1 = -corner.z() - diagonal.z();

	m_shapeType = "box";
}



Cylinder::Cylinder(Vector3f _base, Vector3f _axis, float _r, Material* _mat)
{
	base = _base;
	axis = _axis;
	radius = _r;
	m_pMaterial = _mat;

	m_shapeType = "Cylinder";
}

bool Cylinder::intersect(const Ray &ray, IntersectionData &iD)
{
	Quaternionf q;
	q=Quaternionf::FromTwoVectors(axis,Vector3f::UnitZ());
	Ray new_ray(q._transformVector(ray.Q - base), q._transformVector(ray.D));
	
	Interval interval[3];
	Slab slab(Vector3f(0.0,0.0,1.0),0.0,-sqrtf(axis.x()*axis.x()+axis.y()*axis.y()+axis.z()*axis.z()));
	//end plates intersection
	interval[1].intersect(new_ray,slab);

	float a, b, c;
	a = new_ray.D.x()*new_ray.D.x() + new_ray.D.y()*new_ray.D.y();
	b = new_ray.D.x()*new_ray.Q.x() + new_ray.D.y()*new_ray.Q.y();
	b = 2 * b;
	c = new_ray.Q.x()*new_ray.Q.x() + new_ray.Q.y()*new_ray.Q.y() - radius * radius;

	float det = b * b - 4 * a*c;

	if (det < 0)
		return false;
	else
	{
		float c0, c1;
		c0 = (-b - sqrtf(det))/(2*a);
		c1 = (-b + sqrtf(det))/ (2 * a);
		interval[2].t0 = c0;
		interval[2].t1 = c1;
	}
	float t0, t1;
	t0 = std::max(std::max(interval[0].t0,interval[1].t0),interval[2].t0);
	t1 = std::min(std::min(interval[0].t1,interval[1].t1),interval[2].t1);

	if (t0 > t1)
	{
		// The �of the corner� case
		return false;
	}
	else 
	{
		if (t0 < 0 && t1 < 0)
			return false;
		else if (t0 > 0 && t0 < t1)
		{
			iD.t = t0;
			iD.P = new_ray.Q + iD.t * new_ray.D;
			if (iD.t == interval[1].t0)
			{   //Z
				iD.N = q.conjugate()._transformVector(Vector3f(0,0,1));
			}
			//else if (iD.t == interval[1].t1)
			//{
			//	//-Z
			//	iD.N = iD.N = q.conjugate()._transformVector(Vector3f(0, 0, -1));
			//}
			else 
			{
				/*iD.N = */iD.N = q.conjugate()._transformVector(Vector3f(iD.P.x(), iD.P.y(), 0));
			}
			//theta = atan2f(iD.N.y,iD.N.x);

			return true;
		}
		//else if (t1 > 0 && t1 < t0)
		//{
		//	iD.t = t1;
		//	iD.P = new_ray.Q + iD.t * new_ray.D;
		//	//normals
		//	if (iD.t == interval[1].t1)
		//		{
		//			//-Z
		//			iD.N = iD.N = q.conjugate()._transformVector(Vector3f(0, 0, -1));
		//		}
		//	else
		//	iD.N = interval[1].N1;
		//	return true;
		//}
	}
}

float Cylinder::AreaOfLight()
{
	/*Vector3f height = axis - base;
	float h = sqrtf(height.x*height.x+ height.y*height.y+ height.z*height.z);

	return 2*PI*radius*(radius+h);*/
	return 0.0f;
}

Bbox Cylinder::BoundingBox()
{

	Vector3f B = base + axis;
	Vector3f r(radius,radius,radius);

	auto b1 = Bbox(r + base, -r + base);

	auto b2 = Bbox(r + B, B - r);


	m_boundingBox = Bbox(-r + base, r + B);
	m_boundingBox.extend(b1);
	m_boundingBox.extend(b2);

	return m_boundingBox;
}

Slab::Slab(Vector3f _normal, float _d0, float _d1)
{
	N = _normal;
	d0 = _d0;
	d1 = _d1;
}
