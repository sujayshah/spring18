#pragma once

#include "geom.h"
class Shape;
class Ray;
class Slab;

class IntersectionData
{
public:
	IntersectionData() { shape = nullptr; };
	~IntersectionData() {};

	float t; //Parameter value on ray of the point of intersection
	Vector3f P; //Point of intersection (in world coordinates) 
	Vector3f N; // Normal of surface at intersection point  (in world coordinates) 
	Shape* shape; //object pointer to the shape intersected
	//Vector2f UV;
};

//needed later
class Interval
{
public:
	Interval();
	Interval(float,float);
	Interval(float t0, float t1, Vector3f N0, Vector3f N1);
	~Interval() {};
	void Empty();
	void intersect();
	void intersect(const Ray& r, Slab& s);

	void intersect(const Ray & ray, Slab & slab, bool & inBox);

	float t0, t1;
	Vector3f N0, N1;

};

//-----------------------------------------------------------------------------------------------------------

