#pragma once
// A good quality *thread-safe* Mersenne Twister random number generator.

const float PI = 3.14159f;
// Call myrandom(RNGen) to get a uniformly distributed random number in [0,1].