#include "Ray.h"

Ray::~Ray()
{
}

Ray::Ray(Vector3f _q, Vector3f _d)
{
	Q = _q;
	D = _d;
}

Vector3f Ray::getPoint(float t)
{

//	D.normalize();
	return Q + t * D;
}