#pragma once
#include "geom.h"
class Camera
{
public:
	Camera();
	~Camera();

	Vector3f eye;
	Quaternionf orientation;
	float ry;
	//TODO
	void setCamera(const Vector3f& _eye, const Quaternionf& _o, const float _ry);

};

