#include "Minimizer.h"


Minimizer::Minimizer()
{
}

Minimizer::Minimizer(const Ray & r):ray(r)
{
	tmin = INFINITY;
}

// Called by BVMinimize to intersect the ray with a Shape.  
//This  should return the intersection t, but should also track 
// the minimum t and it's corresponding intersection info. 
// Return INF to indicate no intersection
float Minimizer::minimumOnObject(Shape * obj)
{
	IntersectionData iData;
	if (obj->intersect(ray, iData))
	{
		
		if (iData.t < 0.001f)
			return INFINITY;

		 if (tmin > iData.t)
		{
			iD = iData;
			
			tmin = iData.t;
			iD.shape = obj;
		}
	}
		else
			return INFINITY;
	return iD.t;
}
// Called by BVMinimize to intersect the ray with a Box3d and 
// returns the t value.  This should be similar to the already 
// written box (3 slab) intersection.  (The difference begin a                    
// distance of zero should be returned if the ray starts within the bbox.) 
// Return INF to indicate no intersection
float Minimizer::minimumOnVolume(const Bbox & box)
{

	float t = 0.0f;
	Vector3f L = box.min();  // Box corner lower   
	Vector3f U = box.max();  // Box corner diagonal

	Slab slab[3];
	slab[0].N = Vector3f(1.0, 0.0, 0.0);
	slab[0].d0 = -L.x();
	slab[0].d1 = -L.x() - U.x();

	slab[1].N = Vector3f(0.0, 1.0, 0.0);
	slab[1].d0 = -L.y();
	slab[1].d1 = -L.y() - U.y();

	slab[2].N = Vector3f(0.0, 0.0, 1.0);
	slab[2].d0 = -L.z();
	slab[2].d1 = -L.z() - U.z();
	
	//intersection calculations

	float t0 = 0.0, t1 = INFINITY;
	Interval interval[4];

	interval[0].t0 = t0;
	interval[0].t1 = INFINITY;

	Interval maxt0, mint1;
	for (int i = 1; i <= 3; ++i)
	{
		bool inBox=false;
		interval[i].intersect(ray, slab[i - 1],inBox);
		if (inBox)
		{
			return 0;
		}
	}

	maxt0 = interval[0];
	mint1 = interval[0];
	//last slab max calc problem
	for (int i = 0; i < 3; ++i)
	{
		maxt0 = std::max(interval[i + 1], maxt0, CompareIntervalMaxt0());
		mint1 = std::min(mint1, interval[i + 1], CompareIntervalMint1());
		t0 = maxt0.t0;
		t1 = mint1.t1;
	}
	//t0 max=5.406
	//t1 min =5.593
	if (t0 < 0 && t1 < 0)
		return INFINITY;
	else if (t0 > t1)
		return INFINITY;
	else
	{
		if (t0 > 0 && t0 < t1)
		{
			t = t0;
	/*		iD.P = ray.Q + iD.t * ray.D;
			iD.N = maxt0.N0;*/
		}
		else if (t1 > 0 && t1 < t0)
		{
			t = t1;
			//iD.P = ray.Q + iD.t * ray.D;
			//iD.N = mint1.N1;
		}
		//iD.t = t;
		return t;
	}
	return INFINITY;
}
