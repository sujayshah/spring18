
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows;
using System.Windows.Forms;

namespace mat_290_framework
{
    public partial class MAT290 : Form
    {
        public MAT290()
        {
            InitializeComponent();

            pts_ = new List<Point2D>();
            tVal_ = 0.5F;
            degree_ = 0;
            knot_ = new List<double>();
            EdPtCont_ = true;
            rnd_ = new Random();

            //calculate pascal triangle 
            PascalTri = new long[21, 21];

            for (int y = 0; y < 21; y++)
            {
                int c = 1;
                //for (int q = 0; q < 21 - y; q++)
                //    System.Console.Write("   ");
                
                for (int x = 0; x <= y; x++)
                {
                    //System.Console.Write("        ", c);
                    PascalTri[y, x] = c;
                    c = c * (y - x) / (x + 1);
                }
                //System.Console.WriteLine();
            }
            //System.Console.WriteLine();
        }

        // Point class for general math use
        protected class Point2D : System.Object
        {
            public double x;
            public double y;

            public Point2D(double _x, double _y)
            {
                x = _x;
                y = _y;
            }

            public Point2D(Point2D rhs)
            {
                x = rhs.x;
                y = rhs.y;
            }

            // adds two points together; used for barycentric combos
            public static Point2D operator +(Point2D lhs, Point2D rhs)
            {
                return new Point2D(lhs.x + rhs.x, lhs.y + rhs.y);
            }

            // gets a distance between two points. not actual distance; used for picking
            public static double operator %(Point2D lhs, Point2D rhs)
            {
                double dx = (lhs.x - rhs.x);
                double dy = (lhs.y - rhs.y);

                return (dx * dx + dy * dy);
            }

            // scalar multiplication of points; for barycentric combos
            public static Point2D operator *(double t, Point2D rhs)
            {
                return new Point2D(rhs.x * t, rhs.y * t);
            }

            // scalar multiplication of points; for barycentric combos
            public static Point2D operator *(Point2D rhs, double t)
            {
                return new Point2D(rhs.x * t, rhs.y * t);
            }

            //// scalar multiplication of points; for barycentric combos
            //public static Point2D operator *(Point2D rhs, double t)
            //{
            //    return new Point2D(rhs.x * t, rhs.y * t);
            //}
            // returns the drawing subsytems' version of a point for drawing.
            public System.Drawing.Point P()
            {
                return new System.Drawing.Point((int)x, (int)y);
            }
        };


        List<Point2D> shellPts = new List<Point2D>();//list of shell points
        List<Point2D> pts_; // the list of points used in internal algthms
        double tVal_; // t-value used for shell drawing
        int degree_; // degree of deboor subsplines
        List<double> knot_; // knot sequence for deboor
        bool EdPtCont_; // end point continuity flag for std knot seq contruction
        Random rnd_; // random number generator
        long[,] PascalTri;

        //init for spline interpolation
        double[,] MatEqX;
        double[,] MatEqY;
        double[] OutputX;
        double[] OutputY;

        // pickpt returns an index of the closest point to the passed in point
        //  -- usually a mouse position
        private int PickPt(Point2D m)
        {
            double closest = m % pts_[0];
            int closestIndex = 0;

            for (int i = 1; i < pts_.Count; ++i)
            {
                double dist = m % pts_[i];
                if (dist < closest)
                {
                    closest = dist;
                    closestIndex = i;
                }
            }

            return closestIndex;
        }

       
        private void Menu_Clear_Click(object sender, EventArgs e)
        {

            pts_.Clear();
            shellPts.Clear();
            Menu_Bern.Checked = false;
            Menu_Bern.Enabled = false;
            Menu_DeCast.Checked = false;
            Menu_DeCast.Enabled = false;
            Menu_Midpoint.Checked = false;
            Menu_Midpoint.Enabled = false;
            Refresh();
        }

        private void Menu_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            //if (pts_.Count >= 4)
            //    return;

            //if (pts_.Count > 22)
            //{
            //    MessageBox.Show("Please select less than 22 points");
            //    return;
            //}
            // if the left mouse button was clicked
            if (e.Button == MouseButtons.Left && pts_.Count < 21)
            {
                // add a new point to the controlPoints

                //dummy remove comments
                pts_.Add(new Point2D(e.X, e.Y));

                //HACK::testing for constant points
                //pts_.Add(new Point2D(123, 245));
                //pts_.Add(new Point2D(214, 125));
                //pts_.Add(new Point2D(368, 117));
                //pts_.Add(new Point2D(448, 255));

                degree_ = pts_.Count - 1;
                if (Menu_DeBoor.Checked)
                {
                    ResetKnotSeq();
                    UpdateKnotSeq();
                }

                Refresh();
            }


            if(pts_.Count!=0)
            {
                //uncomment for proj 1 & 2
                //Menu_Bern.Enabled = true;
                //Menu_Midpoint.Enabled = true;
                //Menu_DeCast.Enabled = true;

                Menu_Inter.Enabled = true;
                Menu_Inter_Poly.Enabled = false;
                Menu_Inter_Splines.Enabled = true;
            }

            //commented for now
            // if there are points and the middle mouse button was pressed
            //if (pts_.Count != 0 && e.Button == MouseButtons.Middle)
            //{
            //    // then delete the closest point
            //    int index = PickPt(new Point2D(e.X, e.Y));

            //    pts_.RemoveAt(index);

            //    if (Menu_DeBoor.Checked)
            //    {
            //        ResetKnotSeq();
            //        UpdateKnotSeq();
            //    }

            //    Refresh();
            //}
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            // if the right mouse button is being pressed
            if (pts_.Count != 0 && e.Button == MouseButtons.Right)
            {
                // grab the closest point and snap it to the mouse
                int index = PickPt(new Point2D(e.X, e.Y));

                pts_[index].x = e.X;
                pts_[index].y = e.Y;

                Refresh();
            }
        }
        private void pictureBox1_MouseWheel(object sender, MouseEventArgs e)
        {
            if (Menu_Midpoint.Checked)
                return;

            // if the mouse wheel has moved
            if (e.Delta != 0)
            {
                // change the t-value for shell
                tVal_ += e.Delta / 120 * .02f;

                // handle edge cases
                tVal_ = (tVal_ < 0) ? 0 : tVal_;
                tVal_ = (tVal_ > 1) ? 1 : tVal_;

                Refresh();
            }
        }
       
        private void NUD_degree_ValueChanged(object sender, EventArgs e)
        {
            if (pts_.Count == 0)
                return;

            degree_ = (int)NUD_degree.Value;

            ResetKnotSeq();
            UpdateKnotSeq();

            NUD_degree.Value = degree_;

            Refresh();
        }

        private void CB_cont_CheckedChanged(object sender, EventArgs e)
        {
            EdPtCont_ = CB_cont.Checked;

            ResetKnotSeq();
            UpdateKnotSeq();

            Refresh();
        }

        private void Txt_knot_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == '\n')
            {
                // update knot seq
                string[] splits = Txt_knot.Text.ToString().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                if (splits.Length > pts_.Count + degree_ + 1)
                    return;

                knot_.Clear();
                foreach (string split in splits)
                {
                    knot_.Add(Convert.ToSingle(split));
                }

                for (int i = knot_.Count; i < (pts_.Count + degree_ + 1); ++i)
                    knot_.Add((double)(i - degree_));

                UpdateKnotSeq();
            }

            Refresh();
        }

        private void Menu_Polyline_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        private void Menu_Points_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        private void Menu_Shell_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        private void Menu_DeCast_Click(object sender, EventArgs e)
        {
            Menu_DeCast.Checked = !Menu_DeCast.Checked;
            Menu_Bern.Checked = false;
            Menu_Midpoint.Checked = false;

            Menu_Inter_Poly.Checked = false;
            Menu_Inter_Splines.Checked = false;

            Menu_DeBoor.Checked = false;

            Menu_Polyline.Enabled = true;
            Menu_Points.Enabled = true;
            Menu_Shell.Enabled = true;
            Menu_Shell.Checked = true;
            ToggleDeBoorHUD(false);

            Refresh();
        }

        private void Menu_Bern_Click(object sender, EventArgs e)
        {
            Menu_DeCast.Checked = false;
            Menu_Bern.Checked = !Menu_Bern.Checked;
            Menu_Midpoint.Checked = false;

            Menu_Inter_Poly.Checked = false;
            Menu_Inter_Splines.Checked = false;

            Menu_DeBoor.Checked = false;

            Menu_Polyline.Enabled = true;
            Menu_Points.Enabled = true;
            Menu_Shell.Enabled = false;

            ToggleDeBoorHUD(false);

            Refresh();
        }

        private void Menu_Midpoint_Click(object sender, EventArgs e)
        {
            Menu_DeCast.Checked = false;
            Menu_Bern.Checked = false;
            Menu_Bern.Enabled = false;
            Menu_DeCast.Enabled = false;

            Menu_Midpoint.Checked = !Menu_Midpoint.Checked;

            Menu_Inter_Poly.Checked = false;
            Menu_Inter_Splines.Checked = false;

            Menu_DeBoor.Checked = false;

            Menu_Polyline.Enabled = true;
            Menu_Points.Enabled = true;
            Menu_Shell.Enabled = true;
           
            ToggleDeBoorHUD(false);

            Refresh();
        }

        private void Menu_Inter_Poly_Click(object sender, EventArgs e)
        {
            Menu_DeCast.Checked = false;
            Menu_Bern.Checked = false;
            Menu_Midpoint.Checked = false;

            Menu_Inter_Poly.Checked = !Menu_Inter_Poly.Checked;
            Menu_Inter_Splines.Checked = false;

            Menu_DeBoor.Checked = false;

            Menu_Polyline.Enabled = true;
            Menu_Polyline.Checked = true; 
            Menu_Points.Enabled = true;
            Menu_Shell.Enabled = true;
            Menu_Shell.Checked = false;

            ToggleDeBoorHUD(false);

            Refresh();
        }

        private void Menu_Inter_Splines_Click(object sender, EventArgs e)
        {
            Menu_DeCast.Checked = false;
            Menu_Bern.Checked = false;
            Menu_Midpoint.Checked = false;

            Menu_Inter_Poly.Checked = false;
            Menu_Inter_Splines.Checked = !Menu_Inter_Splines.Checked;

            Menu_DeBoor.Checked = false;
            Menu_Polyline.Enabled = false;
            Menu_Polyline.Checked = false;
            Menu_Points.Enabled = true;
            Menu_Shell.Enabled = false;
            Menu_Shell.Checked = false;

            ToggleDeBoorHUD(false);

            Refresh();
        }

        private void Menu_DeBoor_Click(object sender, EventArgs e)
        {
            Menu_DeCast.Checked = false;
            Menu_Bern.Checked = false;
            Menu_Midpoint.Checked = false;

            Menu_Inter_Poly.Checked = false;
            Menu_Inter_Splines.Checked = false;

            Menu_DeBoor.Checked = !Menu_DeBoor.Checked;

            Menu_Polyline.Enabled = true;
            Menu_Points.Enabled = true;
            Menu_Shell.Enabled = true;

            ToggleDeBoorHUD(true);

            Refresh();
        }

        private void DegreeClamp()
        {
            // handle edge cases
            degree_ = (degree_ > pts_.Count - 1) ? pts_.Count - 1 : degree_;
            degree_ = (degree_ < 1) ? 1 : degree_;
        }

        private void ResetKnotSeq( )
        {
            DegreeClamp();
            knot_.Clear();

            if (EdPtCont_)
            {
                for (int i = 0; i < degree_; ++i)
                    knot_.Add(0.0f);
                for (int i = 0; i <= (pts_.Count - degree_); ++i)
                    knot_.Add((double)i);
                for (int i = 0; i < degree_; ++i)
                    knot_.Add((double)(pts_.Count - degree_));
            }
            else
            {
                for (int i = -degree_; i <= (pts_.Count); ++i)
                    knot_.Add((double)i);
            }
        }

        private void UpdateKnotSeq()
        {
            Txt_knot.Clear();
            foreach (double knot in knot_)
            {
                Txt_knot.Text += knot.ToString() + " ";
            }
        }

        private void ToggleDeBoorHUD( bool on )
        {
            // set up basic knot sequence
            if( on )
            {
                ResetKnotSeq();
                UpdateKnotSeq();
            }

            CB_cont.Visible = on;

            Lbl_knot.Visible = on;
            Txt_knot.Visible = on;

            Lbl_degree.Visible = on;
            NUD_degree.Visible = on;
        }
       
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            // pass the graphics object to the DrawScreen subroutine for processing
            DrawScreen(e.Graphics);
        }



        private void DrawScreen(System.Drawing.Graphics gfx)
        {
            // to prevent unecessary drawing
            if (pts_.Count == 0)
                return;

            // pens used for drawing elements of the display
            System.Drawing.Pen polyPen = new Pen(Color.Gray, 1.0f);
            System.Drawing.Pen shellPen = new Pen(Color.Red, 0.5f);
            System.Drawing.Pen splinePen = new Pen(Color.Black, 1.5f);
            System.Drawing.Brush pointBrush = new SolidBrush(Color.Green);
            System.Drawing.Pen shellPtPen = new Pen(Color.Blue,0.5f);
            polyPen.DashPattern= new float[] { 4.0F, 2.0F };

            if (Menu_Shell.Checked)
            {
                // draw the shell
                if(Menu_DeCast.Checked)
                {
                    shellPts.Clear();
                    DrawShell(gfx, shellPen, pts_, tVal_);
                }   
            }

            if (Menu_Polyline.Checked)
            {
                // draw the control poly
                for (int i = 1; i < pts_.Count; ++i)
                {
                    gfx.DrawLine(polyPen, pts_[i - 1].P(), pts_[i].P());
                }
            }

            if (Menu_Points.Checked)
            {
                // draw the control points
                foreach (Point2D pt in pts_)
                {
                    gfx.FillEllipse(pointBrush,(float) pt.x - 4.0F, (float)pt.y - 4.0F, 8.0F, 8.0F);
                }

                if (Menu_DeCast.Checked)
                {
                    //draw shell pts
                    foreach (Point2D Spts in shellPts)
                    {
                        gfx.DrawEllipse(shellPtPen, (float)Spts.x - 4.0F, (float)Spts.y - 4.0F, 8.0F, 8.0F);
                    }
                }
            }

            //if(pts_.Count==0)
            //{
            //    Menu_Bern.
            //}
            // you can change these variables at will; i have just chosen there
            //  to be six sample points for every point placed on the screen
            double steps = pts_.Count * 6;
            double alpha = 1 / steps;

            ///////////////////////////////////////////////////////////////////////////////
            // Drawing code for algorithms goes in here                                  //
            ///////////////////////////////////////////////////////////////////////////////

            // DeCastlejau algorithm
            if (Menu_DeCast.Checked)
            {
                Point2D current_left;
                Point2D current_right = pts_[0];

                for (double t = alpha; t < 1; t += alpha)
                {
                    current_left = current_right;
                    current_right = DeCastlejau(t);
                    gfx.DrawLine(splinePen, current_left.P(), current_right.P());
                }
                gfx.DrawLine(splinePen, current_right.P(), DeCastlejau(1).P());
            }

            // Bernstein polynomial
            if (Menu_Bern.Checked)
            {
               
                Point2D current_left;

                Point2D current_right = pts_[0];
               // plot.Add(current_right.P());
                for (double t = alpha; t < 1; t += alpha)
                {
                    current_left = current_right;
                    current_right = Bernstein(t);
                    gfx.DrawLine(splinePen, current_left.P(), current_right.P());
                }
                gfx.DrawLine(splinePen, current_right.P(), Bernstein(1).P());
            }

            // Midpoint algorithm
            if (Menu_Midpoint.Checked)
            {
                DrawMidpoint(gfx, splinePen, pts_);
            }

            // polygon interpolation
            if (Menu_Inter_Poly.Checked)
            {
                Point2D current_left;
                Point2D current_right = pts_[0];

                // alpha = 0.6f;
              //  alpha = 0.5f;
                for (double t = alpha; t < degree_; t += alpha)
                {
                    current_left = current_right;
                    current_right = PolyInterpolate(t);
                    gfx.DrawLine(splinePen, current_left.P(), current_right.P());
                }
                gfx.DrawLine(splinePen, current_right.P(), PolyInterpolate(degree_).P());
            }


            // spline interpolation
            if (Menu_Inter_Splines.Checked)
            {
                Point2D current_left;
                Point2D current_right = pts_[0]; //new Point2D(SplineInterpolate(0));

                MatEqX = new double[pts_.Count + 2, pts_.Count + 2];
                MatEqY= new double[pts_.Count + 2, pts_.Count + 2];
                OutputX = new double[pts_.Count + 2];
                OutputY = new double[pts_.Count + 2];

                double[] Xt = new double[pts_.Count + 2];
                double[] Yt = new double[pts_.Count + 2];

                double[] ddX = new double[pts_.Count + 2];
                double[] ddY = new double[pts_.Count + 2];

                for (int i=0;i<pts_.Count+2;++i)
                {
                    Xt[i] = 1;
                    Yt[i] = 1;
                }
                for(int i=0;i<pts_.Count;++i)
                {
                    OutputX[i] = pts_[i].x;
                    OutputY[i] = pts_[i].y;
                }
                OutputX[pts_.Count ] = OutputX[pts_.Count + 1] = 0;
                OutputY[pts_.Count ] = OutputY[pts_.Count + 1] = 0;

                

                ddX[0] = ddX[1]=0;
                ddY[0] = ddY[1]=0;
                ddX[2] = ddY[2] = 2;

                for (int i=3;i< pts_.Count+2;++i)
                {
                    ddX[i] = 6;
                    ddY[i] = 6;
                }


                //calculate fxt and fyt
                //if (pts_.Count < 3)
                //{
                //}
                //else
                {
                    int t = 0;
                    //fill up the matrix
                    for (int i=0;i<pts_.Count+2;++i)
                    {
                        int c = 1;
                       
                        //for 2 extra co-eff
                        if (i >= pts_.Count )
                        {
                            if(i<pts_.Count+1)
                                t = 0;
                            else
                                t = pts_.Count - 1;

                            for (int j = 0; j < pts_.Count + 2; ++j)
                            {
                                c = j - 3;
                                if (j < 4)
                                {
                                    if(j==2)
                                    {
                                        MatEqX[i, j] = ddX[j] * Math.Pow(1, 1);
                                        MatEqY[i, j] = ddY[j] * Math.Pow(1, 1);
                                    }     
                                    else
                                    {
                                        MatEqX[i, j] = ddX[j] * Math.Pow(t, 1);
                                        MatEqY[i, j] = ddY[j] * Math.Pow(t, 1);
                                    }
                                        
                                }     
                                //truncated functions
                                else
                                {
                                    if (t >= c)
                                    {
                                        MatEqX[i, j] = ddX[j] * Math.Pow(t - c, 1);
                                        MatEqY[i, j] = ddY[j] * Math.Pow(t - c, 1);
                                    }
                                    else
                                    {
                                        MatEqX[i, j] = ddX[j] * Math.Pow(0, 1);
                                        MatEqY[i, j] = ddY[j] * Math.Pow(0, 1);
                                    }
                                }
                            }
                            
                        }
                        else
                        {
                            //regular co-efficients
                            for (int j = 0; j < pts_.Count + 2; ++j)
                            {
                                c = j - 3;
                                if (j < 4)
                                {
                                    MatEqX[i, j] = Xt[j] * Math.Pow(t, j);
                                    MatEqY[i, j] = Yt[j] * Math.Pow(t, j);
                                }    
                                //truncated functions
                                else
                                {
                                    if (t >= c)
                                    {
                                        MatEqX[i, j] = Xt[j] * Math.Pow(t - c, 3);
                                        MatEqY[i, j] = Yt[j] * Math.Pow(t - c, 3);
                                    }   
                                    else
                                    {
                                        MatEqX[i, j] = Xt[j] * Math.Pow(0, 3);
                                        MatEqY[i, j] = Yt[j] * Math.Pow(0, 3);
                                    }  
                                }
                            }
                        } 
                        ++t;
                    }
                }

                if(!ComputeCoefficents(MatEqX,OutputX))
                {
                    return;
                }
                if(!ComputeCoefficents(MatEqY, OutputY))
                {
                    return;
                }
                //use calculated fxt,fyt
                //alpha = 0.5;
                for (double t = alpha; t < degree_; t += alpha)
                {
                    current_left = current_right;
                    current_right = SplineInterpolate(t);
                    gfx.DrawLine(splinePen, current_left.P(), current_right.P());
                }

                gfx.DrawLine(splinePen, current_right.P(), SplineInterpolate(degree_).P());
            }
            /*
           // deboor
           if (Menu_DeBoor.Checked && pts_.Count >= 2)
           {
               Point2D current_left;
               Point2D current_right = new Point2D(DeBoorAlgthm(knot_[degree_]));

               double lastT = knot_[knot_.Count - degree_ - 1] - alpha;
               for (double t = alpha; t < lastT; t += alpha)
               {
                   current_left = current_right;
                   current_right = DeBoorAlgthm(t);
                   gfx.DrawLine(splinePen, current_left.P(), current_right.P());
               }

               gfx.DrawLine(splinePen, current_right.P(), DeBoorAlgthm(lastT).P());
           }
           */
            ///////////////////////////////////////////////////////////////////////////////
            // Drawing code end                                                          //
            ///////////////////////////////////////////////////////////////////////////////


            // Heads up Display drawing code

            Font arial = new Font("Arial", 12);

            if (Menu_DeCast.Checked)
            {
                gfx.DrawString("DeCasteljau", arial, Brushes.Black, 0, 30);
            }
            else if (Menu_Midpoint.Checked)
            {
                gfx.DrawString("Midpoint", arial, Brushes.Black, 0, 30);
            }
            else if (Menu_Bern.Checked)
            {
                gfx.DrawString("Bernstein", arial, Brushes.Black, 0, 30);
            }
            else if (Menu_DeBoor.Checked)
            {
                gfx.DrawString("DeBoor", arial, Brushes.Black, 0, 30);
            }

            gfx.DrawString("t-value: " + tVal_.ToString("F"), arial, Brushes.Black, 500, 30);

            gfx.DrawString("t-step: " + alpha.ToString("F6"), arial, Brushes.Black, 600, 30);

            gfx.DrawString(pts_.Count.ToString(), arial, Brushes.Black, 750, 30);

        }

        private void DrawShell(System.Drawing.Graphics gfx, System.Drawing.Pen pen, List<Point2D> pts, double t)
        {
           
            Point2D[] intermediateP = new Point2D[degree_];
            Point2D[] newCntrlPts = new Point2D[degree_ + 1];

            for (int i = 0; i <= degree_; i++)
                newCntrlPts[i] = pts[i];

            int temp = degree_;
            while (temp > 0)
            {
                for (int i = 0; i < temp; ++i)
                    intermediateP[i] = (1 - t) * newCntrlPts[i] + t * newCntrlPts[i + 1];

                //copy intermediates into new cntrl points
                for (int i = 0; i < temp; i++)
                {
                    newCntrlPts[i] = intermediateP[i];
                    shellPts.Add(intermediateP[i]);
                }

                --temp;
            }

            temp = degree_;
            int counter = 0;
            for (int i = 0; i < shellPts.Count - 1; i++)
            {
                if (counter != temp - 1)
                {
                    ++counter;
                    Console.WriteLine("joining " + i +"and "+(i+1));
                    gfx.DrawLine(pen, shellPts[i].P(), shellPts[i + 1].P());

                }
                else
                {
                    Console.WriteLine("not drawing");
                    --temp;
                    counter = 0;
                }
            }

        }

        private Point2D Gamma(int start, int end, double t)
        {
            return new Point2D(0, 0);
        }

        private Point2D DeCastlejau(double t)
        {
            Point2D[] intermediateP = new Point2D[degree_];
            Point2D[] newCntrlPts = new Point2D[degree_+1];

            for (int i = 0; i <= degree_; i++)
                newCntrlPts[i] = pts_[i];

            int temp = degree_;
            while (temp > 0)
            {
                for (int i = 0; i < temp; ++i)
                    intermediateP[i] = (1 - t) * newCntrlPts[i] + t * newCntrlPts[i + 1];

                //copy intermediates into new cntrl points
                 for (int i = 0; i < temp; i++)
                {
                    newCntrlPts[i] = intermediateP[i];
                }
                --temp;
            }
            return intermediateP[0];
        }

        private Point2D Bernstein(double t)
        {
            double x=0.0f,y = 0.0f;
            for (int i = 0; i <pts_.Count; ++i)
            {
                y += (double)(pts_[i].y * PascalTri[degree_, i]
                     * Math.Pow((1 - t), (degree_ - i)) *  Math.Pow(t, i));

                x+= (double)(pts_[i].x * PascalTri[degree_, i]
                     * Math.Pow((1 - t), (degree_ - i)) * Math.Pow(t, i));
            }
            return new Point2D(x, y);
        }

        private const double MAX_DIST = 6.0F;

        private void DrawMidpoint(System.Drawing.Graphics gfx, System.Drawing.Pen pen, List<Point2D> cPs)
        {  
            //double x = 0.0f, y = 0.0f;

            //List<Point2D> intermediate = new List<Point2D>();
            //List<Point2D> midPts = new List<Point2D>();
            ////Mid point algorithm


            //for (int k = 0; k < 5; k++)
            //{
            //    if (k != 0)
            //    {
            //        cPs.Clear();
            //        for (int p = 0; p < intermediate.Count; p++)
            //            cPs.Add(intermediate[p]);
            //    }
            //    intermediate.Clear();
            //    intermediate.Add(cPs[0]);
            //    for (int i = 0; i < cPs.Count - 1; i++)
            //    {
            //        x = (double)((1 - 0.5) * cPs[i].x + 0.5 * cPs[i + 1].x);
            //        y = (double)((1 - 0.5) * cPs[i].y + 0.5 * cPs[i + 1].y);
            //        midPts.Add(new Point2D(x, y));
            //        intermediate.Add(new Point2D(x, y));
            //    }
            //    intermediate.Add(cPs[cPs.Count - 1]);
            //}

            //for (int j = 0; j < cPs.Count - 1; j++)
            //{
            //    gfx.DrawLine(pen, cPs[j].x, cPs[j].y, cPs[j + 1].x, cPs[j + 1].y);
            //}

        }

        private Point2D PolyInterpolate(double t)
        {
            int i, j;
            double x, y;
            double[] input_t = new double[pts_.Count];
            double[] divided_differenceX = new double[pts_.Count];
            double[] divided_differenceY = new double[pts_.Count];
            double[] Fx = new double[pts_.Count];
            double[] Fy = new double[ pts_.Count];
            for (i = 0; i < pts_.Count; ++i)
            {
                //t0,t1,.....
                input_t[i] = i;
                //keep copy of points
                Fx[i] = pts_[i].x;
                Fy[i] = pts_[i].y;
                //populate values with base case
                divided_differenceX[i] = Fx[i];
                divided_differenceY[i] = Fy[i];
            }

            for (i = 0; i < pts_.Count-1; ++i)
            {
                for (j = pts_.Count-1; j > i; --j)
                {
                    divided_differenceX[j] = (divided_differenceX[j - 1] - divided_differenceX[j]) / (input_t[j-1-i]- input_t[j]);
                    divided_differenceY[j] = (divided_differenceY[j - 1] - divided_differenceY[j]) / (input_t[j - 1 - i] - input_t[j]);
                }
            }

            x = divided_differenceX[pts_.Count-1];
            y = divided_differenceY[pts_.Count-1];

            for(i= pts_.Count - 2; i>=0;--i)
            {
                x = divided_differenceX[i] + (t - input_t[i]) * x;
                y= divided_differenceY[i] + (t - input_t[i]) * y;
            }

            return new Point2D(x, y);
        }

        private Point2D SplineInterpolate(double t)
        {
            double x=0, y=0,c=1;
            for(int i=0;i<pts_.Count+2;++i)
            {
                c = i - 3;
                if (i<4)
                {
                    x += OutputX[i] * Math.Pow(t, i);
                    y += OutputY[i] * Math.Pow(t, i);
                }
                else
                {
                    if (t >= c)
                    {
                        x += OutputX[i] * Math.Pow(t - c, 3);
                        y += OutputY[i] * Math.Pow(t - c, 3);
                    }
                    else
                    {
                        x += OutputX[i] * Math.Pow(0, 3);
                        y += OutputY[i] * Math.Pow(0, 3);
                    }
                }
            }

            return new Point2D(x, y);
        }




        private Point2D DeBoorAlgthm(double t)
        {
            return new Point2D(0, 0);
        }

 //------------------------Credits: Moodie Ghaddar----------------------------------------
        bool ComputeCoefficents(double[,] A, double[] B)
        {
            int size = B.Length;
            int sizeMinusOne = size - 1;

            // Row echelon form
            for (int i = 0; i < sizeMinusOne; ++i)
            {
                if (A[i, i] == 0)
                {
                    if (!FindSwappableRow(A, B, i))
                        return false; // No solution
                }

                if (A[i, i] != 1)
                {
                    double coeff = 1 / A[i, i];
                    for (int k = 0; k < size; ++k)
                    {
                        A[i, k] *= coeff;
                    }
                    B[i] *= coeff;
                }

                for (int j = i + 1; j < size; ++j)
                {
                    double coeffeciant = A[j, i] / A[i, i];
                    for (int k = 0; k < size; ++k)
                    {
                        A[j, k] -= coeffeciant * A[i, k];
                    }
                    B[j] -= coeffeciant * B[i];
                }
            }

            if (A[sizeMinusOne, sizeMinusOne] == 0)
                return false; // No solution

            if (A[sizeMinusOne, sizeMinusOne] != 1)
            {
                double coeff = 1 / A[size - 1, size - 1];
                A[sizeMinusOne, sizeMinusOne] *= coeff;
                B[sizeMinusOne] *= coeff;
            }

            // Backward subtitution
            for (int i = sizeMinusOne - 1; i >= 0; --i)
            {
                double coeff = 0;
                for (int j = 0; j < (sizeMinusOne - i); ++j)
                {
                    int k = sizeMinusOne - j;
                    coeff += B[k] * A[i, k];
                }
                B[i] -= coeff;
            }

            return true;
        }

        bool FindSwappableRow(double[,] A, double[] B, int row)
        {
            int start = row + 1;
            double maxValue = 0;
            int bestRowToSwapIndex = row;

            for (int i = start; i < B.Length; ++i)
            {
                if (A[i, row] > maxValue)
                {
                    maxValue = A[i, row];
                    bestRowToSwapIndex = i;
                }
            }

            if (bestRowToSwapIndex == row)
                return false;

            for (int i = 0; i < B.Length; ++i)
            {
                double temp = A[row, i];
                A[row, i] = A[bestRowToSwapIndex, i];
                A[bestRowToSwapIndex, i] = temp;
            }

            double tempB = B[row];
            B[row] = B[bestRowToSwapIndex];
            B[bestRowToSwapIndex] = tempB;

            return true;
        }
//------------------------Credits: Moodie Ghaddar----------------------------------------
    }
}