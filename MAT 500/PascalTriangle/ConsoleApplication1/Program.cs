﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Pascal Triangle Program");

            System.Console.Write("Enter the number of rows: ");

            string input = System.Console.ReadLine();

            int[,] Pascal = new int[21, 21];

            int n = Convert.ToInt32(input);

            for (int y = 0; y < n; y++)
            {
                int c = 1;
                for (int q = 0; q < n - y; q++)
                {
                    System.Console.Write("   ");
                }
                for (int x = 0; x <= y; x++)
                {
                    System.Console.Write("   {0:D} ", c);
                    Pascal[y, x] = c;
                    c = c * (y - x) / (x + 1);
                }
                System.Console.WriteLine();
                System.Console.WriteLine();
            }
            System.Console.WriteLine();
            System.Console.ReadLine();


            //public static BigInteger[][] GetPascalTriangleImproved(int rows)
            //{
            //    BigInteger[][] result = new BigInteger[rows + 1][];
            //    for (int x = 0; x < rows; x++)
            //    {
            //        result[x] = new BigInteger[x + 1];
            //        result[x][0] = 1; // first element is ALWAYS 1

            //        for (int x1 = 1; x1 <= x; x1++)
            //        {
            //            // last element is always 1 (just like the first)
            //            if (x1 == x) { result[x][x] = 1; continue; }

            //            // in all other cases, just add the 2 digits in the upper row
            //            result[x][x1] = result[x - 1][x1 - 1] + result[x - 1][x1];
            //        }
            //    }
            //    return result;
            //}
        }
    }
}
