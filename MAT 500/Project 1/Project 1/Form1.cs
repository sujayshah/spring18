﻿//Sujay Shah  student id:60001517

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_1
{
    public partial class Form1 : Form
    {
        bool polyLine = true;
        bool points = true;
        int degree = 1;
        float t_step = 0.0f;
        float[] P_coeff; 
        Graphics gfx;
        Pen pen;
        Brush blackBrush, greenBrush;
        List<PointF> controlPoints = new List<PointF>();
        bool bbForm=true;
         int scaleX = 600;
         int scaleY = 50;
        bool dragPoint = false;
        int offsetx = 0;
        int offsety = 0;
        int selected_index = -1;
        long [,] PascalTri; 
        public Form1()
        {
            InitializeComponent();
            gfx = drawingArea.CreateGraphics();
            blackBrush= new SolidBrush(Color.Black);
            greenBrush = new SolidBrush(Color.Green);
            pen = new Pen(Color.Black, 1);
            controlPoints.Add(new PointF(0.0f, 1.0f));
            controlPoints.Add(new PointF(1.0f, 1.0f));
            comboBox1.SelectedIndex = 0;

            offsetx = Math.Abs(drawingArea.Location.X - drawingArea.Size.Width / 2);
            offsety = Math.Abs(drawingArea.Location.Y - drawingArea.Size.Height / 2);
            P_coeff = new float[(int)numericUpDown1.Maximum+1];
            P_coeff[0] = 1.0f;
            P_coeff[1] = 1.0f;
            PascalTri = new long[(int)numericUpDown1.Maximum+1,(int) numericUpDown1.Maximum+1];

            //calculate pascal triangle 
            PascalTri = new long[21, 21];

            for (int y = 0; y < 21; y++)
            {
                int c = 1;
                for (int q = 0; q < 21 - y; q++)
                {
                    System.Console.Write("   ");
                }
                for (int x = 0; x <= y; x++)
                {
                    System.Console.Write("        ", c);
                    PascalTri[y, x] = c;
                    c = c * (y - x) / (x + 1);
                }
                System.Console.WriteLine();
            }
            System.Console.WriteLine();
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                bbForm = true;
            }
            else
                bbForm = false;
        }
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlPoints.Clear();
            //drawingArea.Image = null;
            gfx.Clear(Color.Black);
            Refresh();
        }
        public float Bernstein(float[] P, int degree, float t)
        {
            float y = 0.0f;
            for (int i = 0; i <= degree; ++i)
            {
                y += (float)(P[i] * PascalTri[degree, i]
                     * Math.Pow((1 - t), (degree - i)) * Math.Pow(t, i));

            }
            return y;
        }
        public float BernsteinX(List<PointF> cntrlPts, int degree, float t)
        {
            float x = 0.0f;
            for (int i = 0; i <= degree; ++i)
            {
                x += (float)(cntrlPts[i].X * PascalTri[degree, i]
                     * Math.Pow((1 - t), (degree - i)) * Math.Pow(t, i));

            }
            return x;
        }
        private void drawingArea_Paint(object sender, PaintEventArgs e)
        {
            //draw axis
            //drawable area width  = 900
            //              height = 510
            // y axis
            // gfx.DrawLine(pen, 30, 0, 30, drawingArea.Size.Height);
            var gfx = e.Graphics;
                gfx.DrawLine(pen,worldToScreen(new PointF(0, 3)), worldToScreen(new PointF(0, -3)));
            
            // x axis
            // gfx.DrawLine(pen, 30, drawingArea.Size.Height/2, drawingArea.Size.Width, drawingArea.Size.Height/2);
            gfx.DrawLine(pen, worldToScreen(new PointF(0, 0)), worldToScreen(new PointF(1, 0)));
           
            if(Menu_Points.Checked)
            {
                //draw control points
                for (int i=0;i<controlPoints.Count;++i)
                {
                    Point ScreenPoint = worldToScreen(controlPoints[i]);
                    gfx.FillEllipse(greenBrush,ScreenPoint.X-5,ScreenPoint.Y-5,10,10);
                }
       
            }

            if (Menu_PolyLines.Checked)
            {
                //list of points to be plotted for making curve
                List<Point> Plot = new List<Point>();

                for (t_step=0.0f; t_step < 1.0f; t_step += 0.01f)
                {
                    PointF t = new PointF();
                    //drawing algorithm
                    if (bbForm)
                    {
                        t.X = BernsteinX( controlPoints, degree, t_step);
                        t.Y = Bernstein(P_coeff, degree, t_step);
                    }
                    else
                    {
                        t.X = t_step;
                        t.Y = NLI(P_coeff, degree, t.X);
                    }
                    Plot.Add(worldToScreen(t));
                }
                gfx.DrawLines(pen, Plot.ToArray());

            }

            Console.WriteLine("called from paint");
        }

        public float NLI(float[] P, int degree, float t)
        {
            float[] intermediateP= new float[degree]; 
            float[] newCntrlPts = new float[degree+1]; 

            for(int i=0;i<=degree;i++)
            {
                newCntrlPts[i] = P_coeff[i];
            }
            while (degree>0)
            {
                 
                for(int i=0;i<degree;++i)
                {
                    intermediateP[i] = (1 - t) * newCntrlPts[i] + t * newCntrlPts[i + 1];
                }
                //copy intermediates into new cntrl points
                for (int i = 0; i < degree; i++)
                {
                    newCntrlPts[i] = intermediateP[i];
                }
                --degree;
            }
            return intermediateP[0];
        }

        
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            degree = (int)numericUpDown1.Value;
            ReDrawGraph();
        }
        private void ReDrawGraph()
        {
            //clear prev points
            controlPoints.Clear();
            //add new points
            for (int i=0;i<=degree;++i)
            {
                PointF pointf = new PointF(((float)i / degree), 1.0f);
                //PointF pointf = new PointF((float)i+space, 1.0f);
                controlPoints.Add(pointf);
                P_coeff[i] = 1.0f;
            }
            
            Refresh();
        }
        private Point worldToScreen(PointF mathPt)
        {
            Point pixel = new Point(0,0);
            pixel.X = (int)(mathPt.X * scaleX+30);
            pixel.Y = (int)(mathPt.Y*(-scaleY)+drawingArea.Size.Height / 2);
            return pixel;
        }

        private void drawingArea_MouseUp(object sender, MouseEventArgs e)
        {
            dragPoint = false;
            selected_index = -1;
            Refresh();
        }

        private void drawingArea_MouseDown(object sender, MouseEventArgs e)
        {
            dragPoint = true;

            var mouseX = e.X;
            var mouseY = e.Y;
            //select a point 
            //Console.Write("POint Selected");
            for (int i = 0; i < controlPoints.Count; ++i)
            {
                if(Math.Abs(worldToScreen(controlPoints[i]).X - mouseX) < 5 && Math.Abs( worldToScreen(controlPoints[i]).Y - mouseY) < 5)
                {
                    selected_index = i;
                    Console.Write("POint Selected");
                    break;
                }
            }
            //controlPoints
            Refresh();
        }
        
        private void drawingArea_MouseMove(object sender, MouseEventArgs e)
        {
            //if (controlPoints.Count > 1)
            //    scaleX = (int)worldToScreen(controlPoints[degree]).X - worldToScreen(controlPoints[0]).X;
                var currentMousePos = screenToWorld(new Point(0,e.Y));
            if ((selected_index !=-1&&currentMousePos.Y<3)&& (selected_index != -1 && currentMousePos.Y > -3))
            {
                var newX = controlPoints[selected_index].X;
                var point = screenToWorld(new Point(e.X, e.Y));    
                controlPoints[selected_index] = new PointF(point.X, point.Y);
                P_coeff[selected_index] = point.Y;
                Refresh();
            }
        }

        private void pointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private PointF screenToWorld(Point pixel)
        {
            PointF mathPt = new PointF(0, 0);
            mathPt.X = (pixel.X - 30)/(float)scaleX;
            mathPt.Y = (drawingArea.Size.Height / 2 - pixel.Y) / (float)scaleY;
            return mathPt;
        }
    }
}
